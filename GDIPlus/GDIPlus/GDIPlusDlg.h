﻿
// GDIPlusDlg.h: 헤더 파일
//

#pragma once

// For Gdi plus
#include <Gdiplus.h>
#pragma comment(lib, "gdiplus")
using namespace Gdiplus;

// CGDIPlusDlg 대화 상자
class CGDIPlusDlg : public CDialogEx
{
// 생성입니다.
public:
	CGDIPlusDlg(CWnd* pParent = nullptr);	// 표준 생성자입니다.

// 대화 상자 데이터입니다.
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_GDIPLUS_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 지원입니다.


// 구현입니다.
protected:
	HICON m_hIcon;

	// 생성된 메시지 맵 함수
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	// For Gdi Plus
	ULONG_PTR m_gpToken;
public:
	afx_msg void OnDestroy();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);

	void PaintCircle(Graphics& mem);
	void PaintLine(Graphics& mem);
};
