README.md
===

### 빌드환경

* Windows 10
* Visual Studio 2017 (15.9.6)

---

### GDI+ 기본작업

```
// .h 에 다음을 추가

// For Gdi plus
#include <Gdiplus.h>
#pragma comment(lib, "gdiplus")
using namespace Gdiplus;

// 클래스 내부
// For Gdi Plus
ULONG_PTR m_gpToken;

```

```
    // BOOL CGDIPlusDlg::OnInitDialog()
	GdiplusStartupInput gdiplusStartupInput;
	if (GdiplusStartup(&m_gpToken, &gdiplusStartupInput, NULL) != Ok) {
		AfxMessageBox(_T("GDI+ 초기화에 실패했습니다."));
		return FALSE;
	}
```

```
void CGDIPlusDlg::OnDestroy()
{
	CDialogEx::OnDestroy();

	GdiplusShutdown(m_gpToken);

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.
}
```

---

### 더블버퍼링

```
void CGDIPlusDlg::OnPaint()
{
	// GDI plus DoubleBuffer
	CPaintDC dc(this);
	CRect rlClientRect;
	GetClientRect(&rlClientRect);

	Rect rclClient(rlClientRect.left, rlClientRect.top, rlClientRect.Width(), rlClientRect.Height());

	CDC MemDC;
	MemDC.CreateCompatibleDC(&dc);
	CBitmap memBitmap;
	memBitmap.CreateCompatibleBitmap(&dc, rlClientRect.Width(), rlClientRect.Height());
	CBitmap *pOldBitmap = MemDC.SelectObject(&memBitmap);
	Graphics mem(MemDC.m_hDC);
	Gdiplus::SolidBrush bgBrush(Gdiplus::Color(255, 255, 255, 255));
	mem.SetSmoothingMode(SmoothingModeHighQuality);
	mem.FillRectangle(&bgBrush, rclClient); // 배경을 흰색으로 지운다

	// Paint Here

	PaintCircle(mem);
	PaintLine(mem);

	// Paint End

	dc.BitBlt(0, 0, rlClientRect.right, rlClientRect.bottom, &MemDC, 0, 0, SRCCOPY);

	MemDC.SelectObject(pOldBitmap);
	mem.ReleaseHDC(dc.m_hDC);

}
```

```
BOOL CGDIPlusDlg::OnEraseBkgnd(CDC* pDC)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.

	//return CDialogEx::OnEraseBkgnd(pDC);
	return TRUE;
}
```

---

### 원그리기

```
void CGDIPlusDlg::PaintCircle(Graphics& mem)
{
	Pen BlackPen(Color(255, 0, 0, 0), 10.0f);
	mem.DrawEllipse(&BlackPen, 30, 30, 100, 100);
}
```

---

### 선그리기

```
void CGDIPlusDlg::PaintLine(Graphics& mem)
{
	mem.SetSmoothingMode(SmoothingModeAntiAlias);
	Color c(0, 0, 255);
	Pen p(c, 10.0);
	Gdiplus::PointF p1, p2;
	p1.X = 100.0;
	p1.Y = 100.0;
	p2.X = 200.0;
	p2.Y = 200.0;

	mem.DrawLine(&p, p1, p2);

	// 선 끝 처리
	p.SetStartCap(LineCapRoundAnchor);
	p.SetEndCap(LineCapArrowAnchor);

}
```

---

### Rotation

```
Gdiplus::Matrix matrix;
matrix.RotateAt(<원하는 각도>, <Rotation을 하는 기준 포인트>);
mem.SetTransform(&matrix);

// Rotation사용 이후
matrix.reset();
mem.SetTransform(&matrix);
```

---

* [GDI+ 원그리기](https://holyhacker.tistory.com/155)<br>
* [GDI+ 라인그리기](http://pjw0703.blogspot.com/2011/04/mfc-gdi.html)<br>
* [GDI+ Double Buffer](https://eachan.tistory.com/6)<br>
* [GDI+ Rotation](https://redstory2010.tistory.com/157)<br>